
using System.Runtime.Remoting.Messaging;

public class CalculatorClient
{
    IAsyncResult m_AsyncResult;

    public void AsynchAdd()
    {
        Calculator calc = new Calculator();
        DispatchAdd(calc, 2, 3);
        int result = GetResult();
        Debug.Assert(result == 5);
    }
    protected void DispatchAdd(Calculator calc, int num1, int num2)
    {
        BinaryOperation oppDel;
        oppDel = new BinaryOperation(calc.Add);
        m_AsyncResult = oppDel.BeginInvoke(2, 3, null, null);
    }
    protected int GetResult()
    {
        int result = 0;

        AsyncResult asyncResult = (AsyncResult)m_AsyncResult;
        BinaryOperation oppDel = (BinaryOperation)asyncResult.AsyncDelegate;

        Debug.Assert(asyncResult.EndInvokeCalled == false);
        result = oppDel.EndInvoke(m_AsyncResult);
        return result;
    }


    Calculator calc = new Calculator();
    BinaryOperation oppDel;
    oppDel = new BinaryOperation(calc.Add);

    IAsyncResult asyncResult = oppDel.BeginInvoke(2, 3, null, null);

    asyncResult.AsyncWaitHandle.WaitOne(); 

int result;
    result = oppDel.EndInvoke(asyncResult);
Debug.Assert(result == 5);

Calculator calc = new Calculator();
    BinaryOperation oppDel;
    oppDel = new BinaryOperation(calc.Add);

    IAsyncResult asyncResult = oppDel.BeginInvoke(2, 3, null, null);

while (asyncResult.IsCompleted == false)
{
asyncResult.AsyncWaitHandle.WaitOne(10,false); 

int result;
    result = oppDel.EndInvoke(asyncResult); 

Calculator calc = new Calculator();
    BinaryOperation oppDel1;
    BinaryOperation oppDel2;

    oppDel1 = new BinaryOperation(calc.Add);
    oppDel2 = new BinaryOperation(calc.Add);
    IAsyncResult asyncResult1 = oppDel1.BeginInvoke(2, 3, null, null);
    IAsyncResult asyncResult2 = oppDel2.BeginInvoke(4, 5, null, null);

    WaitHandle[] handleArray =
    {asyncResult1.AsyncWaitHandle,asyncResult2.AsyncWaitHandle};

    WaitHandle.WaitAll(handleArray);

int result;
    
    result = oppDel1.EndInvoke(asyncResult1);
Debug.Assert(result == 5);

result = oppDel2.EndInvoke(asyncResult2);
Debug.Assert(result == 9);
    

    }
    
   


